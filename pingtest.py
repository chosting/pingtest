# Simple script to ping target and capture latency
# Latency is then reported to influxDB on remote server
# Script should be run from crontab at the desited interval

import os
import socket
import time
from datetime import datetime


influxdbServer = '192.168.1.16'
pingTarget = "www.google.com"
dbName = "miscmonitor"
sleepInterval = 300 # in seconds

while True:
    pingResult = os.popen('ping -q -c 50 ' + pingTarget + ' -W 5').read()
    # Now split output into lines and add to list
    pingResultList = pingResult.splitlines()
    packetLossLine = pingResultList[3]

    packetLoss = packetLossLine.split(' ')
    packetLossPercentage = packetLoss[5].strip("%")
    if packetLossPercentage != '100.0':
        # Get Average Latency 
        pingLatencyLine = pingResultList[4] 
        pingLatencyLineList = pingLatencyLine.split(' ')
        pingLatencyValues = pingLatencyLineList[3].split('/')
        pingLatencyAverage = pingLatencyValues[1]

    # Now build http post request 
    hostname = socket.gethostname()    
    IPAddr = socket.gethostbyname(hostname)    

    sendDataLatency = "curl -i -XPOST 'http://" + influxdbServer + ":8086/write?db=" + dbName + "' --data-binary 'internet_latency,host=" + hostname + " value=" + pingLatencyAverage + "'"
    sendDataPacketLoss = "curl -i -XPOST 'http://" + influxdbServer + ":8086/write?db=" + dbName + "' --data-binary 'internet_pktloss,host=" + hostname + " value=" + packetLossPercentage + "'"

    now = datetime.now()
    currentTime = now.strftime("%H:%M:%S")
    print("Current Time =", currentTime)
    print(currentTime + ' ' + sendDataLatency)
    print(currentTime + ' ' + sendDataPacketLoss)
    os.system(sendDataLatency)
    os.system(sendDataPacketLoss)
    time.sleep(sleepInterval)
